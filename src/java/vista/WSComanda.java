/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import control.IServicioMenu;
import control.IServicioUsuario;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import modelo.Menu;
import modelo.Usuario;

/**
 *
 * @author alumno
 */
@WebService(serviceName = "WSComanda")
public class WSComanda {

    @EJB
    private IServicioMenu servicioMenu;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "crearMenu")
    public Menu crearMenu(@WebParam(name = "menu") Menu menu) {
        return servicioMenu.crearMenu(menu);
    }

    @WebMethod(operationName = "editarMenu")
    public Menu editarMenu(@WebParam(name = "menu") Menu menu) {
        return servicioMenu.editarMenu(menu);
    }

    @WebMethod(operationName = "elimnarMenu")
    @Oneway
    public void elimnarMenu(@WebParam(name = "menu") Menu menu) {
         servicioMenu.eliminarMenu(menu);
    }

    @WebMethod(operationName = "listarMenu")
    public List<Menu> listarMenu() {
        return servicioMenu.listarMenu();
    }

    @EJB
    private IServicioUsuario servicioUsuario;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "crearUsuario")
    public Usuario crearUsuario(@WebParam(name = "usuario") Usuario usuario) {
        return servicioUsuario.crearUsuario(usuario);
    }

    @WebMethod(operationName = "editarUsuario")
    public Usuario editarUsuario(@WebParam(name = "usuario") Usuario usuario) {
        return servicioUsuario.editarUsuario(usuario);
    }

    @WebMethod(operationName = "elimnarUsuario")
    @Oneway
    public void elimnarUsuario(@WebParam(name = "usuario") Usuario usuario) {
         servicioUsuario.eliminarUsuario(usuario);
    }

    @WebMethod(operationName = "listarUsuario")
    public List<Usuario> listarUsuario() {
        return servicioUsuario.listarUsuario();
    }

}
