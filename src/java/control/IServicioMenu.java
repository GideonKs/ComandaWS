/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.List;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import modelo.Menu;

/**
 *
 * @author alumno
 */
@Remote
public interface IServicioMenu {
    public Menu crearMenu(Menu menu);
    public Menu editarMenu(Menu menu);
    public void eliminarMenu(Menu menu);
    public List<Menu> listarMenu();
    
}
