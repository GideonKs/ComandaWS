/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.List;
import javax.ejb.Remote;
import modelo.Usuario;

/**
 *
 * @author alumno
 */
@Remote
public interface IServicioUsuario {
    
    public Usuario crearUsuario(Usuario usuario);
    public Usuario editarUsuario(Usuario usuario);
    public void eliminarUsuario(Usuario usuario);
    public List<Usuario> listarUsuario();
    
}
