/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import javax.ejb.Remote;

/* Conjunto de métodos que son implementados en otras clases*/
/**
 *
 * @author alumno
 */
@Remote
public interface IRemota {

    public String holaMundo(String nombre);
    
}
