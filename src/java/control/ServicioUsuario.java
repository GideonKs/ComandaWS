/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import modelo.Menu;
import modelo.Usuario;

/**
 *
 * @author alumno
 */
@Stateless
public class ServicioUsuario extends Persistencia implements IServicioUsuario{
//        @PersistenceContext(unitName = "ComandaWSPU")
//         public EntityManager em;
    @Override
    public Usuario crearUsuario(Usuario usuario){
        this.em.persist(usuario);
        return usuario;
        
        
    }
    @Override
    public Usuario editarUsuario(Usuario usuario){
        this.em.merge(usuario);
        return usuario;
    }
    
    @Override
    public void eliminarUsuario(Usuario usuario){
        this.em.remove(usuario);
                
    }
    @Override
     public List<Usuario> listarUsuario(){
        Query consulta = this.em.createNamedQuery("menu.Listar");
        return consulta.getResultList();
    }    
    
}
    

