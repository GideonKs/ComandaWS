/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import modelo.Menu;

/**
 *
 * @author alumno
 */
@Stateless
public class ServicioMenu extends Persistencia implements IServicioMenu{
    
    @Override
    public Menu crearMenu(Menu menu){
        this.em.persist(menu);
        return menu;
    }
    @Override
    public Menu editarMenu(Menu menu){
        this.em.merge(menu);
        return menu;
    }
   @Override
   public void eliminarMenu(Menu menu){
       this.em.remove(menu);
        
    }
    @Override
    public List<Menu> listarMenu(){
        Query consulta = this.em.createNamedQuery("menu.Listar");
        return consulta.getResultList();
    }
    
}
