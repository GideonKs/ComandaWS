/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author DOCENTE
 */
@Entity
@Table(name = "plato")
@NamedQueries({
    @NamedQuery(name = "plato.Listar",
            query = "select p from Plato p")
})
public class Plato implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre", length = 50)
    private String nombre;
    @Column(name = "precio", precision = 10, scale = 2)
    private BigDecimal precio;
    @ManyToOne
    @JoinColumn(name = "pertencemenu", referencedColumnName = "id")
    private Menu perteneceMenu;
    @OneToMany(mappedBy = "tienePlato")
    private List<DetalleComanda> perteneceDetalleComanda;

    public Plato() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Menu getPerteneceMenu() {
        return perteneceMenu;
    }

    public void setPerteneceMenu(Menu perteneceMenu) {
        this.perteneceMenu = perteneceMenu;
    }

    public List<DetalleComanda> getPerteneceDetalleComanda() {
        if (perteneceDetalleComanda == null) {
            perteneceDetalleComanda = new ArrayList<>();
        }
        return perteneceDetalleComanda;
    }

    public void setPerteneceDetalleComanda(List<DetalleComanda> perteneceDetalleComanda) {
        this.perteneceDetalleComanda = perteneceDetalleComanda;
    }
    
    public void mostrar(){
        System.out.println(nombre + " : " + perteneceMenu.getNombre());
    }
}
