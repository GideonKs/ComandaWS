/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DOCENTE
 */
@Entity
@Table(name = "detallecomanda")
public class DetalleComanda implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "cantidad")
    private byte cantidad;
    @ManyToOne
    @JoinColumn(name = "tieneplato", referencedColumnName = "id")
    private Plato tienePlato;
    @ManyToOne
    @JoinColumn(name = "pertenececomanda", referencedColumnName = "id")
    private Comanda perteneceComanda;

    public DetalleComanda() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte getCantidad() {
        return cantidad;
    }

    public void setCantidad(byte cantidad) {
        this.cantidad = cantidad;
    }

    public Plato getTienePlato() {
        return tienePlato;
    }

    public void setTienePlato(Plato tienePlato) {
        this.tienePlato = tienePlato;
    }

    public Comanda getPerteneceComanda() {
        return perteneceComanda;
    }

    public void setPerteneceComanda(Comanda perteneceComanda) {
        this.perteneceComanda = perteneceComanda;
    }

}
