/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author DOCENTE
 */
@Entity
@Table(name = "menu")
@NamedQueries({
@NamedQuery(name = "menu.Listar",
        query = "select m from Menu m")})
public class Menu implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre", length = 150)
    private String nombre;
    
    @OneToMany(mappedBy = "perteneceMenu", fetch = FetchType.LAZY)
    private List<Plato> tienePlato;

    public Menu() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Plato> getTienePlato() {
        if (tienePlato == null) {
            tienePlato = new ArrayList<>();
        }
        return tienePlato;
    }

    public void setTienePlato(List<Plato> tienePlato) {
        this.tienePlato = tienePlato;
    }
    
    public void mostrar(){
        System.out.println(nombre);
        for (Plato plato : tienePlato) {
            plato.mostrar();
        }
    }
    
}
