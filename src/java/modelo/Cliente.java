/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author DOCENTE
 */
@Entity
@Table(name = "cliente")
public class Cliente implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre", length = 150)
    private String nombre;
    @Column(name = "dni", length = 9)
    private String dni;
    @OneToMany(mappedBy = "tieneCliente")
    private List<Comanda> tieneComanda;

    public Cliente() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public List<Comanda> getTieneComanda() {
        if (tieneComanda == null) {
            tieneComanda = new ArrayList<>();
        }
        return tieneComanda;
    }

    public void setTieneComanda(List<Comanda> tieneComanda) {
        this.tieneComanda = tieneComanda;
    }
    
}
