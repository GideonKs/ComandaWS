/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author DOCENTE
 */
@Entity
@Table(name = "comanda")
public class Comanda implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @ManyToOne
    @JoinColumn(name = "tienecliente", referencedColumnName = "id")
    private Cliente tieneCliente;
    @ManyToOne
    @JoinColumn(name = "pertenecemozo", referencedColumnName = "id")
    private Mozo perteneceMozo;
    @ManyToOne
    @JoinColumn(name = "atiendemeza", referencedColumnName = "id")
    private Mesa atiendeMeza;
    @OneToMany(mappedBy = "perteneceComanda")
    private List<DetalleComanda> tieneDetalleComanda;

    public Comanda() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getTieneCliente() {
        return tieneCliente;
    }

    public void setTieneCliente(Cliente tieneCliente) {
        this.tieneCliente = tieneCliente;
    }

    public Mozo getPerteneceMozo() {
        return perteneceMozo;
    }

    public void setPerteneceMozo(Mozo perteneceMozo) {
        this.perteneceMozo = perteneceMozo;
    }

    public Mesa getAtiendeMeza() {
        return atiendeMeza;
    }

    public void setAtiendeMeza(Mesa atiendeMeza) {
        this.atiendeMeza = atiendeMeza;
    }

    public List<DetalleComanda> getTieneDetalleComanda() {
        if (this.tieneDetalleComanda == null) {
            this.tieneDetalleComanda = new ArrayList<>();
        }
        return tieneDetalleComanda;
    }

    public void setTieneDetalleComanda(List<DetalleComanda> tieneDetalleComanda) {
        this.tieneDetalleComanda = tieneDetalleComanda;
    }
    
}
