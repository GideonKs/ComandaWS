/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author DOCENTE
 */
@Entity
@Table(name = "mesa")
@NamedQueries({
    @NamedQuery(name = "mesa.Listar",
            query = "select m from Mesa m")
})
public class Mesa implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "numero")
    private int numero;
    @Column(name = "capacidad")
    private byte capacidad;
    @ManyToMany
    private List<Mozo> esAtendidaMozo;
    @OneToMany(mappedBy = "atiendeMeza")
    private List<Comanda> esAtendidaComanda;

    public Mesa() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public byte getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(byte capacidad) {
        this.capacidad = capacidad;
    }

    public List<Mozo> getEsAtendidaMozo() {
        if (esAtendidaMozo == null) {
            esAtendidaMozo = new ArrayList<>();
        }
        return esAtendidaMozo;
    }

    public void setEsAtendidaMozo(List<Mozo> esAtendidaMozo) {
        this.esAtendidaMozo = esAtendidaMozo;
    }

    public List<Comanda> getEsAtendidaComanda() {
        if (esAtendidaComanda == null) {
            esAtendidaComanda = new ArrayList<>();
        }
        return esAtendidaComanda;
    }

    public void setEsAtendidaComanda(List<Comanda> esAtendidaComanda) {
        this.esAtendidaComanda = esAtendidaComanda;
    }
    
}
