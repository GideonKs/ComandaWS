/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author DOCENTE
 */
@Entity
@Table(name = "mozo")
@NamedQueries({
    @NamedQuery(name = "mozo.Listar",
            query = "select m from Mozo m"),
})
public class Mozo implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre", length = 100)
    private String nombre;
    @Column(name = "dni", length = 9)
    private String dni;
    @ManyToMany(mappedBy = "esAtendidaMozo")
    private List<Mesa> atiendeMesa;
    @OneToMany(mappedBy = "perteneceMozo")
    private List<Comanda> tieneComanda;

    public Mozo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public List<Mesa> getAtiendeMesa() {
        if (atiendeMesa == null) {
            atiendeMesa = new ArrayList<>();
        }
        return atiendeMesa;
    }

    public void setAtiendeMesa(List<Mesa> atiendeMesa) {
        this.atiendeMesa = atiendeMesa;
    }

    public List<Comanda> getTieneComanda() {
        if (tieneComanda == null) {
            tieneComanda = new ArrayList<>();
        }
        return tieneComanda;
    }

    public void setTieneComanda(List<Comanda> tieneComanda) {
        this.tieneComanda = tieneComanda;
    }
    
}
